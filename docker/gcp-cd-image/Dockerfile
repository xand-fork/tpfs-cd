FROM google/cloud-sdk:slim

RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
# Annoying since this image is based on buster, but a kubernetes-buster does not exist. cloud-sdk-buster doesn't have
# anything other than 1.19
RUN echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update
RUN apt-get install -yqq kubectl=1.25.5-00 unzip dc zip build-essential wget

# Install Vault
ENV VAULT_VERSION="1.2.2"

RUN curl -O -L https://releases.hashicorp.com/vault/$VAULT_VERSION/vault_${VAULT_VERSION}_linux_amd64.zip && \
    unzip vault_*_linux_amd64.zip && \
    mv vault /usr/bin/vault && \
    chmod u+x /usr/bin/vault && \
    rm vault_*_linux_amd64.zip

# Install kustomize
ENV KUSTOMIZE_VERSION="4.5.7"

RUN curl -O -L https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv$KUSTOMIZE_VERSION/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
  tar xvf kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz && \
  mv kustomize /usr/bin/kustomize && \
  chmod u+x /usr/bin/kustomize

# Install JQ based on https://github.com/stedolan/jq/wiki/Installation#with-docker
ENV JQ_VERSION='1.5'

RUN curl -s -L https://raw.githubusercontent.com/stedolan/jq/master/sig/jq-release.key -o /tmp/jq-release.key && \
    curl -s -L https://raw.githubusercontent.com/stedolan/jq/master/sig/v${JQ_VERSION}/jq-linux64.asc -o /tmp/jq-linux64.asc && \
    curl -s -L https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 -o /tmp/jq-linux64 && \
    gpg --import /tmp/jq-release.key && \
    gpg --verify /tmp/jq-linux64.asc /tmp/jq-linux64 && \
    cp /tmp/jq-linux64 /usr/bin/jq && \
    chmod +x /usr/bin/jq && \
    rm -f /tmp/jq-release.key && \
    rm -f /tmp/jq-linux64.asc && \
    rm -f /tmp/jq-linux64

# Install kubeval
ENV KUBEVAL_VERSION='v0.16.1'

RUN curl -O -L https://github.com/instrumenta/kubeval/releases/download/$KUBEVAL_VERSION/kubeval-linux-amd64.tar.gz && \
    tar xvf kubeval-linux-amd64.tar.gz && \
    mv kubeval /usr/local/bin && \
    rm kubeval-linux-amd64.tar.gz

COPY ./devCA.pem /etc/ssl/certs/devCA.pem
COPY ./prodCA.pem /etc/ssl/certs/prodCA.pem
