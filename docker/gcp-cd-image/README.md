<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Gitlab CD / CD Pipeline updating](#gitlab-cd--cd-pipeline-updating)
  - [Dependencies](#dependencies)
  - [Deploy](#deploy)
    - [Build & push](#build--push)
    - [Build only](#build-only)
    - [Push only](#push-only)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Gitlab CD / CD Pipeline updating

The build script is defined in the `../build_and_push_ci_images_if_not_exists.sh`.

## Dependencies

First, install and configure the Google Cloud (GCP) SDK from [here](https://cloud.google.com/sdk/docs/).

Set Docker as GCP's credential helper;
```
gcloud auth configure-docker
```
Then, install the Docker credential helper:
```
gcloud components install docker-credential-gcr
```
>If this is not successful, use methods described on [GCP's site](https://cloud.google.com/container-registry/docs/advanced-authentication).

Configure Docker to use your Container Registry credentials (one time only):
```shell script
docker-credential-gcr configure-docker
```
[Troubleshooting](https://cloud.google.com/container-registry/docs/advanced-authentication)

## Deploy

### Build & push

Retrieve the development CA from google cloud storage then it will be added as a trusted certificate authority within the new docker image.

```
gsutil cp gs://xand-dev-certs/CertificateAuthority/devCA.pem ./
```

Build and push with the following commands:

```
source ../.env
docker build --no-cache -t $TPFS_GCP_CD_IMG .
docker push $TPFS_GCP_CD_IMG
```

### Build only
Test before pushing - _does not update our live deployment_

```
source ../.env
docker build --no-cache -t $TPFS_GCP_CD_IMG .
```

### Push only
If you need to re-push (e.g. if authentication failed)
```
source ../.env
docker push $TPFS_GCP_CD_IMG
```
