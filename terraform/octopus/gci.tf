data "google_compute_image" "octopus_image" {
  family = "transparent-octopus"
  project = var.project_id
}
