# TPFS continuous deployment repository

Summary: This repo contains definitions of machines/images/VMs for use in our deployment systems. Primarily this
is Octopus, but nothing precludes the docker image from being useful in other deployment systems.

At a high level, here are the definitions:
1. **GCP-CD-Image** docker image - Containers from these images are instantiated on the Octopus worker and the
   Octopus deployment runs inside them. They are not Octopus specific because they can be instantiated by
   any deployment system and used for deployment.
1. Octopus Packer Definition - creates Octopus worker VM image in GCP
1. Octopus terraform definition - Provisions instances of the Octopus worker VM into GCP


## Environments
Currently, there are two environments: `Prod` and `Dev`. Environments come into play with the Packer images, and Terraform
apply, but the Docker images are environment agnostic. The Development Octopus worker is for deploying all our test
environments, but does not have access to the Production clusters. Only the Production worker should have access to
the production GKE clusters (there are a few of them, all located in the `xand-prod` gcloud project). 

When making changes to Octopus workers, we suggest first applying and testing in Development. After development passes, then
deploy changes to Production.

## GCP-CD-IMAGE Docker Image

This image is intended to run deployments into Google Cloud. It has the necessary tools and libraries
installed to configure k8s definitions (kustomize) with secrets from vault and push the definitions to a GKE cluster.

#### FYI: The docker file `docker/gcp-cd-image/DockerFile` defines an image that has:
1. Google Cloud SDK installed for GCloud deployment
1. Kubernetes CTL tool for GKE   
1. Basic tools for unzipping, running make scripts, and doing json queries (JQ) from the shell
1. Vault tools, and SSL Certs for getting/setting secrets during a deployment
1. Kustomize for hydrating kubernentes definitions from k8s kustomize templates
1. Kubeval command line for verifying k8s definitions

#### How to publish new revisions of the docker image:
1. Bump semantic version in `.env` file
1. Make your updates to file `Dockerfile`
1. Test your changes locally using `docker build` and `docker run` commands
1. Use Git to commit and push changes to Gitlab
1. GitLab job automatically detects changes in `docker/` folder and will run 
   `build_and_push_ci_images_if_not_exists.sh` to publish the build to our GCR.IO container registry.

## Octopus packer definition

This creates an image in GCloud, it doesn't deploy a VM. The Octopus worker image family is 
maintained by Packer, and there are separate images for production and development. The packer definition file is 
`packer/octopus/octopus.json`, and file `povision-octopus.sh` is part of the packer process that sets up 
the image by installing the Octopus 
tentacle service, and some other dependencies. The packer images are versioned by a timestamp, and
running packer will generate a new image in the family `transparent-octopus`. Past images are not deleted.
Terraform is configured to always take the latest packer image version from the family.

The image is intended to be created manually from a dev workstation, not automatically in CI.

####  How to publish a new Packer Octopus worker image

**Note:** there are two paths below, one for setting up development, and another for production:

1. Install GCloud command line tools and authenticate to your project:  
   1. Production Gcloud project: `xand-prod`
   1. Development Gcloud project: `xand-dev`
1. Install packer   
1. Set the default GCP creds:   
   `gcloud auth application-default revoke` (it's ok if this fails)  
   `gcloud auth application-default login`
1. Make your changes to the `octopus.json` and the provisioning script
1. In your terminal, change dir to packer json folder:  
   `cd packer/octopus`
1. Build the image:  
   1. Production: `packer build -var '../../prod-config.json' octopus.json`
   1. Development: `packer build -var '../../dev-config.json' octopus.json`
1. A new image is created in gcloud with a timestamp version: Open browser to Gcloud console -> 
   Project Xand Development -> Compute Engine -> Images. Look for your image named `transparent-octopus-<timestamp>`
1. Commit and push changes to gitlab.

## Octopus terraform definition

The Terraform definitions deploy an Octopus worker VM, based on the package image. The definitions are setup to deploy
either production or development. This must be running manually on a developer workstation, as CI is not setup to run this.

#### How to initialize Terraform state
There is a special script to initialize your terraform state in `terraform/octopus/scripts/init-tf-env.sh`
that must be run before applying new terraform definitions. Terraform stored state in the cloud the first time these
definitions were applied (each env has it's own state: dev/prod). Your local state is most likely out of sync with cloud
state, and Terraform will not run if you state is our of sync. This process will get your local state in sync with cloud state:

1. Install GCloud command line tools and authenticate to your GCloud project:
   1. Production project: `xand-prod`
   1. Development project: `xand-dev`
1. Install Terraform and JQ
1. Change to Terraform octopus folder:  
   `cd terraform/octopus`
1. Initialize/update your terraform state:  
   1. Production: `./scripts/init-tf-env.sh ../../prod-config.json` 
   1. Development: `./scripts/init-tf-env.sh ../../dev-config.json`

#### How to apply Terraform changes
These definitions will deploy/re-deploy/change the Octopus worker per the definitions.  It apply changes based on a diff
of your terraform definitions and the state. Vault is required to be setup and connected via IAP tunnel.

1. Setup and connect to vault per your environment. There are instructions in the company wiki for connecting to both
   [Prod](https://transparentinc.atlassian.net/wiki/spaces/XNS/pages/1972535297/Production+Vault) and
   [Dev](https://transparentinc.atlassian.net/wiki/spaces/XNS/pages/394133511/Connecting+To+Vault)
1. Make changes to the Terraform definitions as needed
1. If you are updating the base image, then no changes may be required, as a new deploy should pickup the latest 
   version of the image-family (if it has been published to correct gcloud project)
1. Run Terraform plan to review changes (`plan` is a dry-run)
   1. Production: `terraform plan -var-file="../../prod-config.json"`
   1. Development: `terraform plan -var-file="../../dev-config.json"`
1. Commit changes to Gitlab and get MR sign off (This is a suggestion. I could see bypassing this for dev, but no so much for prod)
1. Run Terraform apply
   1. Production: `terraform apply -var-file="../../prod-config.json"`
   1. Development: `terraform apply -var-file="../../dev-config.json"`

